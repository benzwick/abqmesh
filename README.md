# AbqMesh for SALOME

AbqMesh is a SALOME plugin for handling Abaqus and CalculiX mesh files.

It can be used to export a mesh to an `.inp` file as shown here:

![AbqMesh demo](demo.gif)

## Installation

Please read the SALOME documentation on how to
[Extend SALOME gui functions using python plugins](https://docs.salome-platform.org/latest/gui/GUI/using_pluginsmanager.html)
before attempting to install this plugin.

1. Download or clone the files (including Git submodules) into the
   `~/.config/salome/Plugins/abqmesh`
   directory, e.g:

    ```
    cd ~/.config/salome/Plugins
    git clone https://gitlab.com/benzwick/abqmesh.git
    git submodule update --init --recursive
    ```

2. To make the plugin available in SALOME,
   add the following lines to the file
   `~/.config/salome/Plugins/salome_plugins.py`
   (create the file if it doesn't exist):

    ```python
    import salome_pluginsmanager

    # AbqMesh: Abaqus and CalculiX mesh export
    # Imports
    import abqmesh.plugin
    from qtsalome import QIcon
    # Setup
    icon_dir = os.path.join(os.path.dirname(abqmesh.__file__), 'icons')
    # Add functions to plugins menu and toolbar
    salome_pluginsmanager.AddFunction(
        'Export INP file',
        'Export mesh as Abaqus or CalculiX .inp format.',
        abqmesh.plugin.exportSelectedToINP,
        icon=QIcon(os.path.join(icon_dir, 'export_inp.png')))
    salome_pluginsmanager.AddFunction(
        'Remove elements by type',
        'Remove elements from the selected mesh depending on their type.',
        abqmesh.plugin.removeElementsFromSelectedByType)
    ```

## Usage

To export a mesh using the AbqMesh plugin:

1. Select the mesh to export.

2. Click on **Tools** &rarr; **Plugins** &rarr; **Export INP file**.

3. Select the file name in the dialog and click **Save**.

The `abqmesh` modules can also be used in Python scripts,
for example:

```python
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New()

import abqmesh.io

mesh = smesh.Mesh(salome.myStudy.FindObject("Mesh_1").GetObject())
filename = 'mymesh.inp'
abqmesh.io.exportToINP(filename, mesh)
```

## Development

If you find a bug or want new features to be added,
please open an
[issue](https://gitlab.com/benzwick/abqmesh/-/issues).
Before filing a new issue,
please review the currently open issues
to make sure that you aren't creating a duplicate.
If you have additional information on an issue,
you can add it to the existing ticket.

The nodal connectivity of SALOME, Abaqus and CalculiX elements is described
[here](./connectivity.md).

During development it may be helpful to prepend the following lines to the file
`~/.config/salome/Plugins/salome_plugins.py`
(see above) to keep the modules updated:

```python
# Modules are loaded when Salome is started so reload in case they have changed
import abqmesh.io
import abqmesh.plugin
import abqmesh.tools
import importlib
importlib.reload(abqmesh.io)
importlib.reload(abqmesh.plugin)
importlib.reload(abqmesh.tools)
```

For more information, please refer to the SALOME documentation:

- [Extend SALOME gui functions using python plugins](https://docs.salome-platform.org/latest/gui/GUI/using_pluginsmanager.html).

- [Mesh: Python interface](https://docs.salome-platform.org/latest/gui/SMESH/smeshpy_interface.html)

- [Mesh: Plugins](https://docs.salome-platform.org/latest/gui/SMESH/tools.html#plugins)

- [Using SALOME GUI python interface](https://docs.salome-platform.org/latest/gui/GUI/text_user_interface.html#using-salome-gui-python-interface)

- [Python Interpreter](https://docs.salome-platform.org/latest/gui/GUI/working_with_python_scripts.html#python-interpreter)

## License

AbqMesh is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

AbqMesh is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
