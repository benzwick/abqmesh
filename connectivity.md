# Nodal connectivity of elements

For 3D elements Salome uses clockwise numbering whereas
Abaqus and CalculiX use anti-clockwise numbering.

See the following links for more information:

[comment]: # (SMESH/share/doc/salome/gui/SMESH/connectivity.html)

- [Salome: Nodal connectivity of elements](https://docs.salome-platform.org/latest/gui/SMESH/connectivity.html)

- [CalculiX: Element Types](https://web.mit.edu/calculix_v2.7/CalculiX/ccx_2.7/doc/ccx/node25.html)

## Edge2 - edge, linear

                            o-----------o       X---- 1
    Salome 'Edge'           1           2
    Abaqus/CalculiX 'B31'   1           2

## Edge3 - edge, quadratic

                            o-----o-----o       X---- 1
    Salome 'Quad_Edge'      1     3     2
    Abaqus/CalculiX 'B32'   1     2     3

## Tri3 - triangle, linear

    Salome 'Triangle' and Abaqus/CalculiX 'S3'

       3
       o
       | \             2
       |   \           |
       |     \         |
       o-------o       X---- 1
       1       2

## Tri6 - triangle, quadratic

    Salome 'Quad_Triangle' and Abaqus/CalculiX 'S6'

       3
       o
       | \
       |   \
     6 o     o 5         2
       |       \         |
       |         \       |
       o-----o-----o     X---- 1
       1     4     2

## Quad4 - quadrangle, linear

    Salome 'Quadrangle' and Abaqus/CalculiX 'S4

                            2
     4 o-------o 3          |
       |       |            |
       |       |            X---- 1
       |       |
     1 o-------o 2

## Quad8 - quadrangle, quadratic

    Salome 'Quad_Quadrangle' and Abaqus/CalculiX 'S8'

             7
     4 o-----o-----o 3          2
       |           |            |
       |           |            |
     8 o           o 6          X---- 1
       |           |
       |           |
     1 o-----o-----o 2
             5

## Tet4 - tetrahedron, linear

    Salome 'Tetra'                  Abaqus/CalculiX 'C3D4'

               4                            4
               o                            o
              /|\                          /|\                3
             / | \                        / | \               |
            /  |  \                      /  |  \              |
         1 o...|...o 2                1 o...|...o 3           X---- 2
            \  |  /                      \  |  /               \
             \ | /                        \ | /                 1 (out of page)
              \|/                          \|/
               o                            o
               3                            2

## Tet10 - tetrahedron, quadratic

    Salome 'Quad_Tetra'             Abaqus/CalculiX 'C3D10'

               4                            4
               o                            o
              /|\                          /|\
             / | \                        / | \
          8 o  |  o 9                  8 o  |  o 10           3
           /   o 10\                    /   o 9 \             |
          /  5 |    \                  /  7 |    \            |
       1 o....o|.....o 2            1 o....o|.....o 3         X---- 2
          \    |    /                  \    |    /             \
           \   |   /                    \   |   /               1 (out of page)
          7 o  |  o 6                  5 o  |  o 6
             \ | /                        \ | /
              \|/                          \|/
               o                            o
               3                            2

## Hex8 - hexahedron, linear

    Salome 'Hexa'                   Abaqus/CalculiX 'C3D8'

        6 o-------------o 7              8 o-------------o 7
         /:            /|                 /:            /|
        / :           / |                / :           / |
       /  :          /  |               /  :          /  |
    5 o-------------o 8 |            5 o-------------o 6 |
      |   :         |   |              |   :         |   |      3
      | 2 o.........|...o 3            | 4 o.........|...o 3    | 2 (into page)
      |  /          |  /               |  /          |  /       |/
      | /           | /                | /           | /        X---- 1
      |/            |/                 |/            |/
    1 o-------------o 4              1 o-------------o 2

## Hex20 - hexahedron, quadratic

     Salome 'Quad_Hexa'             Abaqus/CalculiX 'C3D20'

                  14                              15
         6 o------o------o 7             8 o------o------o 7
          /:            /|                /:            /|
      13 o :           o 15           16 o :           o 14
        18 o          /  o 19           20 o          /  o 19
     5 o------o------o 8 |           5 o------o------o 6 |
       |   :  16  10 |   |             |   :  13  11 |   |      3
       | 2 o......o..|...o 3           | 4 o......o..|...o 3    | 2 (into page)
    17 o  /          o 20           17 o  /          o 18       |/
       | o 9         | o 11            | o 12        | o 10     X---- 1
       |/            |/                |/            |/
     1 o------o------o 4             1 o------o------o 2
             12                               9
