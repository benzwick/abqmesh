# Copyright (C) 2020 Benjamin Zwick
#
# This file is part of AbqMesh, a plugin for the SALOME platform.
#
# AbqMesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AbqMesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AbqMesh.  If not, see <https://www.gnu.org/licenses/>.

import SMESH
from salome.smesh import smeshBuilder
smesh = smeshBuilder.New()


def get_selected_mesh():
    """Get the selected mesh."""
    from salome.gui import helper
    mesh_ref = helper.getSObjectSelected()[0].GetObject()
    mesh = smesh.Mesh(mesh_ref)
    return mesh


def removeElementsByType(mesh,
                         dry_run=False,
                         remove_edges=False,
                         remove_faces=False,
                         remove_cells=False,
                         remove_group_members=False):
    """Delete elements from the mesh depending on their type.

    This is useful for removing edges and faces from meshes
    that do not require this information,
    or cells that do not belong to any groups.

    Removing elements also renumbers the remaining elements.

    Parameters
    ----------
    mesh : salome.smesh.smeshBuilder.Mesh
        Mesh to be modified.
    dry_run : bool, optional
        Perform a trial run with no changes made, returning
        the number of elements that would have been removed.
    remove_edges : bool, optional
        Remove edge elements.
    remove_faces : bool, optional
        Remove face elements.
    remove_cells : bool, optional
        Remove volume elements (cells).
    remove_group_members : bool
        Remove elements even if they belong to a group.

    Returns
    -------
    list(int) or dict or False
        The ids of the elements that were removed,
    or a Dict of the number of elements of each type
    that would have been removed
    if this was not a dry run,
    or False if no elements are to be removed.
    """

    # Get all the element IDs that might be deleted
    elems = []
    if remove_edges:
        edges = mesh.GetElementsByType(SMESH.EDGE)
    else:
        edges = []
    elems += edges
    if remove_faces:
        faces = mesh.GetElementsByType(SMESH.FACE)
    else:
        faces = []
    elems += faces
    if remove_cells:
        cells = mesh.GetElementsByType(SMESH.VOLUME)
    else:
        cells = []
    elems += cells

    # Check which elements belong to at least one group
    if not remove_group_members:
        elems = set(elems)
        groups = mesh.GetGroups()
        for group in groups:
            elems -= set(group.GetIDs())
        elems = list(elems)

    # Remove elements if there are any to be removed
    num_elems = len(elems)
    if num_elems == 0:
        print("AbqMesh: WARNING: No elements found to remove.")
        return False
    elif not dry_run:
        if mesh.RemoveElements(elems):
            print("AbqMesh: Successfully removed {} elements from mesh.".format(num_elems))
            return elems
        else:
            raise RuntimeError("AbqMesh encountered an unexpected error " +
                               "while trying to remove elements.")
    else:
        # The number of elements that would have been removed
        return {
            'edges': len(edges),
            'faces': len(faces),
            'cells': len(cells),
        }

    # FIXME: This does not quite work. Need to interact with view to update.
    # Update the study and viewer
    smesh.UpdateStudy()
    smesh.UpdateView()
    # Update groups
    # for group in groups:
    #     group.Update()
    # Update viewer
