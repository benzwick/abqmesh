# Copyright (C) 2020 Benjamin Zwick
#
# This file is part of AbqMesh, a plugin for the SALOME platform.
#
# AbqMesh is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AbqMesh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AbqMesh.  If not, see <https://www.gnu.org/licenses/>.

import os
from qtsalome import *


class ExportWindow(QWidget):
    """Main window for selecting the mesh filename and export options.
    """
    def __init__(self):
        super(ExportWindow, self).__init__()

        self.setWindowTitle("Export mesh using AbqMesh plugin")
        icon_file = os.path.join(os.path.dirname(__file__), 'icons', 'export_inp.png')
        self.setWindowIcon(QIcon(icon_file))

        # File dialog
        self.fileDialog = QFileDialog(self)
        self.fileDialog.setOption(QFileDialog.DontUseNativeDialog)
        self.fileDialog.setWindowFlags(Qt.Widget)
        self.fileDialog.setAcceptMode(QFileDialog.AcceptSave)

        # Filename
        self.fileDialog.setNameFilters(['Abaqus/CalculiX mesh files (*.inp)',
                                        'All files (*)'])
        self.fileDialog.setDefaultSuffix('inp')

        # Options
        self.compact = QCheckBox("Compact file (no padding between columns)")
        self.hyper = QCheckBox("Hypermesh-like file format")

        # Window layout
        layout = QVBoxLayout()
        layout.addWidget(self.fileDialog)
        layout.addWidget(self.compact)
        layout.addWidget(self.hyper)
        self.setLayout(layout)


class RemoveElementsElementsByTypeWindow(QDialog):
    """Main window for selecting the element types to remove.
    """
    def __init__(self, mesh_name):
        super(RemoveElementsElementsByTypeWindow, self).__init__()

        self.setWindowTitle("Remove elements")

        label = QLabel()
        label.setText("Which elements do you want to remove " +
                      "from the mesh '{}'?".format(mesh_name))

        # Options
        self.remove_edges = QCheckBox("Remove edges (1D elements)")
        self.remove_faces = QCheckBox("Remove faces (2D elements)")
        self.remove_cells = QCheckBox("Remove cells (3D elements)")
        self.remove_group_members = QCheckBox(
            "Remove elements even if they belong to a group")

        # Buttons
        buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)

        # Window layout
        layout = QVBoxLayout()
        layout.addWidget(label)
        layout.addWidget(self.remove_edges)
        layout.addWidget(self.remove_faces)
        layout.addWidget(self.remove_cells)
        layout.addWidget(self.remove_group_members)
        layout.addWidget(buttons)
        self.setLayout(layout)


def getSelectedMesh(context):
    """Get the selected mesh.

    Only one mesh can be selected, and it must be a mesh.

    Parameters
    ----------
    context :
        The SALOME context.

    Returns
    -------
    salome.smesh.smeshBuilder.Mesh or bool
        The selected mesh or ``False`` if no mesh was selected.
    """

    import salome
    from salome.smesh import smeshBuilder
    smesh = smeshBuilder.New()

    # Caption to use for gui objects
    caption = 'AbqMesh'

    # Get study from context
    study = context.study

    # Get selection (and ensure that it is a mesh)
    selCount = salome.sg.SelectedCount()
    if selCount == 0:
        msg = "Nothing selected. Please select a mesh and try again."
        QMessageBox.warning(None, caption, msg)
        return False
    elif selCount > 1:
        msg = "More than one item selected. Please select a mesh and try again."
        QMessageBox.warning(None, caption, msg)
        return False
    else:
        sid = salome.sg.getSelected(0)
        obj = study.FindObjectID(sid).GetObject()
        if not isinstance(obj, salome.smesh.smeshBuilder.meshProxy):
            err = True
        else:
            try:
                obj.Load()
                #print(type(obj))
                mesh = smesh.Mesh(obj)
                #print(type(mesh))
                err = False
            except AttributeError:
                err = True
        if err or not isinstance(mesh, salome.smesh.smeshBuilder.Mesh):
            msg = "Selection is not a mesh. Please select a mesh and try again."
            QMessageBox.warning(None, caption, msg)
            return False

    # Success
    return mesh


def removeElementsFromSelectedByType(context):

    import salome
    from salome.smesh import smeshBuilder
    smesh = smeshBuilder.New()

    import abqmesh.tools

    # Get the mesh
    mesh = getSelectedMesh(context)
    if not mesh:
        return

    # Get filename and options for exporting the mesh
    mesh_name  = smeshBuilder.GetName(mesh)
    window = RemoveElementsElementsByTypeWindow(mesh_name)
    window.show()
    if window.exec_():
        remove_edges = bool(window.remove_edges.checkState())
        remove_faces = bool(window.remove_faces.checkState())
        remove_cells = bool(window.remove_cells.checkState())
        remove_group_members = bool(window.remove_group_members.checkState())
    else:
        return

    # Query how many elements will be removed
    num_elems = abqmesh.tools.removeElementsByType(
        mesh,
        dry_run=True,
        remove_edges=remove_edges,
        remove_faces=remove_faces,
        remove_cells=remove_cells,
        remove_group_members=remove_group_members)

    if not num_elems:
        msg = "No elements to be removed."
        QMessageBox.warning(None, 'AbqMesh', msg)
        return False

    msg = "Delete the following number of elements?\n\n"
    msg += "Edges:   {}\n".format(num_elems['edges'])
    msg += "Faces:   {}\n".format(num_elems['faces'])
    msg += "Cells:   {}\n".format(num_elems['cells'])
    doit = QMessageBox.question(
        None,
        'AbqMesh',
        msg,
        QMessageBox.Yes | QMessageBox.No)

    if doit == QMessageBox.Yes:
        # Delete the elements
        print("AbqMesh: Removing elements {} from '{}'".format(num_elems, mesh_name))
        abqmesh.tools.removeElementsByType(
            mesh,
            remove_edges=remove_edges,
            remove_faces=remove_faces,
            remove_cells=remove_cells,
            remove_group_members=remove_group_members)

    if salome.sg.hasDesktop():
        salome.sg.updateObjBrowser()


def exportSelectedToINP(context):
    """Export the selected mesh to Abaqus or Calculix INP file.
    """

    from salome.smesh import smeshBuilder
    smesh = smeshBuilder.New()

    import abqmesh.io

    mesh = getSelectedMesh(context)
    if not mesh:
        return

    # Get filename and options for exporting the mesh
    window = ExportWindow()
    window.fileDialog.selectFile(smeshBuilder.GetName(mesh) + '.inp')
    window.show()
    if window.fileDialog.exec_():
        fname = window.fileDialog.selectedFiles()[0]
        compact = bool(window.compact.checkState())
        hyper = bool(window.hyper.checkState())
    else:
        return

    print('AbqMesh: Exporting mesh to file: {}'.format(fname))

    abqmesh.io.exportToINP(
        fname,
        mesh,
        compact=compact,
        hyper=hyper)
